<div class="page-header"><h1>{{ $title }}</h1></div>
<blockquote>
    <div class="row">
        <div class="col-sm-10">
            <code>{{ $function }}</code>
        </div>
        <div class="col-sm-2">
            <a href="{{ $url }}" target="_blank" class="pull-right">
                php.net <span class="glyphicon glyphicon-new-window"></span>
            </a>
        </div>
    </div>
</blockquote>
<hr/>
