=============================
PHP password_hash online tool
=============================

This online tool is designed to assist you in the development process when you have to work with PHP password hashes.

Built on top of the Laravel Framework.

Currently hosted at https://php-password-hash-online-tool.herokuapp.com